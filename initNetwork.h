/*_________________________________________________________________________________

Fonction init IP :	Permet d'initialiser l'addresse IP du reseau principal
			afin de pouvoir le sectionner en plusieurs sous reseaux.
			Elle récupere la saisie de l'utilisateur et associe
			cette saisie à la variable qui appelle la fonction.


Fonction Calc Mask :	Permet de calculer le masque de reseau pour le reseau
			principal en prenant en compte le saisie de l'utilisateur
			pour l'écriture de l'adresse IP avec la notation CIDR.
____________________________________________________________________________________ */

using namespace std;

//------------------------------------------------------------
//	Initialisation de l'adresse IP reseaux pincipale
//------------------------------------------------------------

int init_IP()
{
	int A;
	cout<<"octet > ";
	cin>>A;
	return A;
}

//------------------------------------------------------------------
//	Calcul du masque de sous reseaux en fonction de son IP
//------------------------------------------------------------------

string calcul_mask(int test)
{
        string mask;

        switch ( test ) 
        {
        case 8:
                mask  = "255.0.0.0";
        break;

        case 16:
                mask = "255.255.0.0";
        break;

        case 24:
                mask = "255.255.255.0";
        break;
        }

        return mask;
}
