/*_________________________________________________________________________________

Fonction calcul_nbBit : Permet de calculer le nombre de bit nécessaire pour
			le calcul du nombre de sous réseaux que l'utilisateur
			souhaiterait obtenir.
___________________________________________________________________________________ */

using namespace std;

//-----------------------------------
//	Calcul du nombre de bit
//-----------------------------------

int calcul_nbBit(int reseau)
{
	int sr = 0;
        int nb = 1;

        do
        {
                nb = nb * 2;
                sr++;
        }while(nb < reseau);

        return sr;

}


int calcul_machine(int machine)
{
        int pc = machine + 1;
        int nb = 0;

        while(pc > machine)
        {
                pc = pow(2,(8-nb));
                if(pc>machine)
                        nb++;
        }

        if (machine > pc)
        {
                pc = pow(2,(8-(nb-1)));
        }

        return pc;
}
