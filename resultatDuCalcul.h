/*_________________________________________________________________________________

Fonction afficher :     Permet l'affichage des plages d'adresses IP pour les
			sous réseaux calculer via le choix de l'utilisateur.
____________________________________________________________________________________ */

using namespace std;

//----------------------------------------------------
//      Affichage du tableau par rapport au cidr
//----------------------------------------------------

void afficher(string mask, int byteOne, int byteTwo, int byteThree, int byteFour, int cidr, int nbBit, int nbsousR, int saut, int maskSR, const int thebyte[8] )
{
	int i(0); // variable i pour les boucles itératives

	// Affichage de l'entête du tableau de plage
        cout<<"\n\n  plage \t\t ip/cidr\t\t last address\t\t hôte \n"<<endl;

	if (mask == "255.0.0.0")
        {

                byteTwo = 0;
                byteThree = 0;
                byteFour = 0;
                do
                {
                        i++;
                        cout<<" plage "<<i<<" :\t"<<byteOne<<"."<<byteTwo<<"."<<byteThree<<"."<<byteFour<<" / "<<cidr<<"\t\t";
                        byteTwo = byteTwo + saut;
                        cout<<byteOne<<"."<<byteTwo-1<<"."<<"255"<<"."<<"255"<<"\t\t";
                        cout<<byteOne<<" . "<<byteTwo-saut<<"-"<<byteTwo-1<<" . "<<"0-255"<<" . "<<"0-255"<<endl;
                }while(i<nbsousR);

                for(i=0;i<nbBit;i++)
                {
                        maskSR = maskSR + thebyte[i];
                }

                cout<<"\n netmask  :\t "<<"255."<<maskSR<<".0.0\n\n"<<endl;

        }


	if (mask == "255.255.0.0") 
        {       byteThree = 0;
                byteFour = 0;
                do
                {
                        i++;
                        cout<<" plage "<<i<<" :\t"<<byteOne<<"."<<byteTwo<<"."<<byteThree<<"."<<byteFour<<" / "<<cidr<<"\t\t";
                        byteThree = byteThree + saut;
                        cout<<byteOne<<"."<<byteTwo<<"."<<byteThree-1<<"."<<"255"<<"\t\t";
                        cout<<byteOne<<"."<<byteTwo<<" . "<<byteThree-saut<<"-"<<byteThree-1<<" . "<<"0-255"<<endl;
                }while(i<nbsousR);

                for(i=0;i<nbBit;i++)
                {
                        maskSR = maskSR + thebyte[i];
                }
                
                cout<<"\n netmask  :\t "<<"255.255."<<maskSR<<".0\n\n"<<endl;
        }

	 if (mask == "255.255.255.0")
        {
                byteFour = 0;
                do 
                {
                        i++;
                        cout<<" plage "<<i<<" :\t"<<byteOne<<"."<<byteTwo<<"."<<byteThree<<"."<<byteFour<<" / "<<cidr<<"\t\t";
                        byteFour = byteFour + saut;
                        cout<<byteOne<<"."<<byteTwo<<"."<<byteThree<<"."<<byteFour-1<<"\t\t";
                        cout<<byteOne<<"."<<byteTwo<<"."<<byteThree<<"."<<byteFour-saut+1<<" - "<<byteFour-2<<endl;
                }while(i<nbsousR);

                for(i=0;i<nbBit;i++)
                {
                        maskSR = maskSR + thebyte[i];
                }
                
                cout<<"\n netmask  :\t "<<"255.255.255."<<maskSR<<"\n\n"<<endl;
        }
}


