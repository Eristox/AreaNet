//--------------------------------------------------------------------------------------------
//                      Author : Eristox
//                      Version : 6.1
//                      Day : 19/04/2016
//--------------------------------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <math.h>
#include "initNetwork.h"
#include "calculSousReseau.h"
#include "resultatDuCalcul.h"
#include "resultatDuCalculD.h"

using namespace std;

int main ()
{
	system("clear"); // netoyage de la console

	//----------------------------------------
	//	initialisation des Variables
	//----------------------------------------

	const int thebyte[8] = {128,64,32,16,8,4,2,1}; // Tableau de représentation d'un octet par ses pussance de 2
        int byteOne, byteTwo, byteThree, byteFour; // Variable "byteNum" : représentation des octets formant une addresse IP
        int cidr; // Représentation de la notation CIDR
        string mask; // Chaîne de caractere pour les masques de reseaux
        int nbsousR; // Nombre de sous reseaux
        int nbBit; // Nombres de bits à puiser pour le calcul de sous reseau
        int saut; // Saut de bits pour le calcul des plage de sous reseaux en fonction des sous reseaux demander
        int i(0); // Pas pour les boucles de tableau
        int maskSR(0); // Calcul du masque de sous reseaux
	bool choix = true;

	//-----------------------------------------------------------------------------------------
        //	Initialisation de l'adresse IP pour chacques octets via la fonction init_IP() 
	//	et enregistrement de la notation CIDR et calcul du masque de reseau
	//-----------------------------------------------------------------------------------------

        byteOne = init_IP();
        byteTwo = init_IP();
        byteThree = init_IP();
        byteFour = init_IP();

	cout<<"\n@IP/CIDR : "<<byteOne<<"."<<byteTwo<<"."<<byteThree<<"."<<byteFour<<" / ";
	cin>>cidr;

	mask = calcul_mask(cidr);
	cout<<"masque : "<<mask<<"\n"<<endl;


	//----------------------------------------------------
	//	Enregistrement du choix de l'utilisateur
	//----------------------------------------------------

	cout<<"Comment souhaitez vous effectuez votre recherche : "<<endl;
	cout<<"\t - Par nombre de sous reseaux ( 0 )"<<endl;
	cout<<"\t - Par nombre de machine à connecter ( 1 )"<<endl;
	cout<<" votre choix -> ";
	cin>>nbsousR;

	switch(nbsousR)
	{
	case 0:
		choix = true;
	break;

	case 1:
		choix = false;
	break;
	}


	//----------------------------------------------------------------------------------
        //      Calcul du nombre de sous reseaux en fonction du choix de l'utilisateur
        //----------------------------------------------------------------------------------

	if ( choix == true )
	{
		cout<<"\nnombre de sous réseaux souhaité : ";
		cin>>nbsousR;

		nbBit =  calcul_nbBit(nbsousR);
		nbsousR = pow(2,nbBit);
		saut = pow(2,(8 - nbBit));
		cidr = cidr + nbBit; // nouvelle valeur pour la notaion CIDR en fonction du nombre de sous reseau souhaité par l'utilisateur


		afficher(mask, byteOne, byteTwo, byteThree, byteFour, cidr, nbBit, nbsousR, saut, maskSR, thebyte);
	}

	if (choix == false)
	{
                cout<<"\nnombre de machine à connecter : ";
                cin>>nbsousR;

		saut = calcul_machine(nbsousR);
	        cidr = cidr + nbBit;

	        afficherD(mask, byteOne, byteTwo, byteThree, byteFour, cidr, nbBit, nbsousR, saut, maskSR, thebyte);
	}
	return 0;
}

